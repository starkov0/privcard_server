__author__ = 'pierrestarkov'
from rest_framework import serializers


class SQuestionSerializer(serializers.Serializer):
    id = serializers.IntegerField(allow_null=True)
    question = serializers.CharField()
    answer = serializers.CharField()
    answer_key = serializers.CharField()