__author__ = 'pierrestarkov'
from rest_framework import serializers


class AccountSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()
    password_key = serializers.CharField()