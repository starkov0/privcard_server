__author__ = 'pierrestarkov'
from rest_framework import serializers


class CategoryIdSerializer(serializers.Serializer):
    id = serializers.IntegerField(allow_null=True)


class FileContainerIdSerializer(serializers.Serializer):
    id = serializers.IntegerField(allow_null=True)


class FileIndexIdSerializer(serializers.Serializer):
    id = serializers.IntegerField(allow_null=True)


class DirectoryIdSerializer(serializers.Serializer):
    id = serializers.IntegerField(allow_null=True)


class CategorySerializer(serializers.Serializer):
    id = serializers.IntegerField(allow_null=True)
    name = serializers.CharField(allow_blank=True, allow_null=True)
    directory_set = DirectoryIdSerializer(many=True)
    filecontainer_set = FileContainerIdSerializer(many=True)


class DirectorySerializer(serializers.Serializer):
    id = serializers.IntegerField(allow_null=True)
    name = serializers.CharField(allow_blank=True, allow_null=True)
    category = CategoryIdSerializer(allow_null=True)
    head_directory = DirectoryIdSerializer(many=True)
    filecontainer_set = FileContainerIdSerializer(many=True)


class FileContainerSerializer(serializers.Serializer):
    id = serializers.IntegerField(allow_null=True)
    name = serializers.CharField(allow_blank=True)
    category = CategoryIdSerializer(allow_null=True)
    directory = DirectoryIdSerializer()
    fileindex_set = FileIndexIdSerializer(many=True)


class FileIndexSerializer(serializers.Serializer):
    id = serializers.IntegerField(allow_null=True)
    index = serializers.CharField(allow_blank=True)
    file = serializers.CharField(allow_blank=True)
    category = CategoryIdSerializer(allow_null=True)
    filecontainer = FileContainerIdSerializer(allow_null=True)