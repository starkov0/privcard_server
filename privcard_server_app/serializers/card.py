__author__ = 'pierrestarkov'
from rest_framework import serializers
from privcard_server_app.serializers.content import CategorySerializer


class CardSerializer(serializers.Serializer):
    id = serializers.IntegerField(allow_null=True)
    name = serializers.CharField()
    username = serializers.CharField()
    password = serializers.CharField()
    password_key = serializers.CharField()
    key_password = serializers.CharField()
    category_set = CategorySerializer(many=True)


class CardCardSerializer(serializers.Serializer):
    id = serializers.IntegerField(allow_null=True)
    username = serializers.CharField()
    password_key = serializers.CharField()