__author__ = 'pierrestarkov'
from django.db import models
from django.contrib.auth.hashers import (check_password, make_password, )


class SQuestion(models.Model):
    question = models.TextField()
    answer = models.TextField()
    answer_key = models.TextField()
    account = models.ForeignKey('Account')

    def set_answer(self, raw_answer):
        self.answer = make_password(raw_answer)

    def check_answer(self, raw_answer):
        return check_password(raw_answer, self.answer)