__author__ = 'pierrestarkov'
from django.db import models
from django.contrib.auth.models import AbstractBaseUser


class Auth(AbstractBaseUser):
    username = models.TextField(unique=True)
    USERNAME_FIELD = 'username'