__author__ = 'pierrestarkov'
from django.db import models


class Directory(models.Model):
    name = models.TextField()
    directory = models.ForeignKey('Directory', null=True, related_name='head_directory')
    category = models.ForeignKey('Category')