__author__ = 'pierrestarkov'
from django.db import models


class Category(models.Model):
    name = models.TextField()
    account = models.ForeignKey('Account', null=False)
    card_set = models.ManyToManyField('Card')