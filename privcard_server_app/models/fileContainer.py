__author__ = 'pierrestarkov'
from django.db import models


class FileContainer(models.Model):
    name = models.TextField()
    directory = models.ForeignKey('Directory', null=True)
    category = models.ForeignKey('Category')