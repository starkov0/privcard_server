__author__ = 'pierrestarkov'
from django.db import models
from privcard_server_app.models.auth import Auth
from django.contrib.auth.hashers import (check_password, make_password, )


class Account(Auth):
    password_key = models.TextField()
