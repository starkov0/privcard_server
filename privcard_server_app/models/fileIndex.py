__author__ = 'pierrestarkov'
from django.db import models


class FileIndex(models.Model):
    index = models.IntegerField()
    file = models.TextField()
    category = models.ForeignKey('Category')
    filecontainer = models.ForeignKey('FileContainer')