__author__ = 'pierrestarkov'
from django.db import models
from privcard_server_app.models.auth import Auth


class Card(Auth):
    name = models.TextField()
    password_key = models.TextField()
    key_password = models.TextField()
    account = models.ForeignKey('Account', null=False)