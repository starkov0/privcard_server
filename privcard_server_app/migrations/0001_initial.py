# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Auth',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('username', models.TextField(unique=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Directory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.TextField()),
                ('category', models.ForeignKey(to='privcard_server_app.Category')),
                ('directory', models.ForeignKey(related_name='head_directory', to='privcard_server_app.Directory', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='FileContainer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.TextField()),
                ('category', models.ForeignKey(to='privcard_server_app.Category')),
                ('directory', models.ForeignKey(to='privcard_server_app.Directory', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='FileIndex',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('index', models.IntegerField()),
                ('file', models.TextField()),
                ('category', models.ForeignKey(to='privcard_server_app.Category')),
                ('filecontainer', models.ForeignKey(to='privcard_server_app.FileContainer')),
            ],
        ),
        migrations.CreateModel(
            name='SQuestion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question', models.TextField()),
                ('answer', models.TextField()),
                ('answer_key', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Account',
            fields=[
                ('auth_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('password_key', models.TextField()),
            ],
            options={
                'abstract': False,
            },
            bases=('privcard_server_app.auth',),
        ),
        migrations.CreateModel(
            name='Card',
            fields=[
                ('auth_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('name', models.TextField()),
                ('password_key', models.TextField()),
                ('key_password', models.TextField()),
                ('account', models.ForeignKey(to='privcard_server_app.Account')),
            ],
            options={
                'abstract': False,
            },
            bases=('privcard_server_app.auth',),
        ),
        migrations.AddField(
            model_name='squestion',
            name='account',
            field=models.ForeignKey(to='privcard_server_app.Account'),
        ),
        migrations.AddField(
            model_name='category',
            name='account',
            field=models.ForeignKey(to='privcard_server_app.Account'),
        ),
        migrations.AddField(
            model_name='category',
            name='card_set',
            field=models.ManyToManyField(to='privcard_server_app.Card'),
        ),
    ]
