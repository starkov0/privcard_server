__author__ = 'pierrestarkov'
from privcard_server_app.models.auth import Auth


def authenticate(username, password):
    try:
        auth = Auth.objects.get(username=username)
        if auth.check_password(raw_password=password):
            return auth
    except:
        pass
    return None
