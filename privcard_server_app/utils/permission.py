__author__ = 'pierrestarkov'
from rest_framework.permissions import BasePermission
from privcard_server_app.utils.query import auth_get_account, auth_get_card


class IsAccount(BasePermission):

    def has_permission(self, request, view):
        try:
            auth_get_account(auth=request.user)
            return True
        except:
            return False


class IsCard(BasePermission):

    def has_permission(self, request, view):
        try:
            auth_get_card(auth=request.user)
            return True
        except:
            return False