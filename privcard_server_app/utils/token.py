__author__ = 'pierrestarkov'
from rest_framework.authtoken.models import Token


def remove_token(user):
    Token.objects.get(user=user).delete()


def create_token(user):
    return Token.objects.create(user=user)


def update_token(user):
    remove_token(user=user)
    return create_token(user=user)