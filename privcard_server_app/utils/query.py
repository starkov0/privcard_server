__author__ = 'pierrestarkov'
from privcard_server_app.models.account import Account
from privcard_server_app.models.card import Card
from privcard_server_app.models.category import Category
from privcard_server_app.models.fileContainer import FileContainer
from privcard_server_app.models.directory import Directory
from privcard_server_app.models.squestion import SQuestion


# AUTH
def auth_get_account(auth):
    try:
        return Account.objects.get(id=auth.id)
    except:
        return None


def auth_get_card(auth):
    try:
        return Card.objects.get(id=auth.id)
    except:
        return None


# ACCOUNT
def get_account(username):
    try:
        return Account.objects.get(username=username)
    except:
        return None


def account_get_squestion(pk_squestion, account):
    try:
        return SQuestion.objects.get(id=pk_squestion,
                                     account=account)
    except:
        return None


def account_get_card(account, pk_card):
    try:
        return Card.objects.get(id=pk_card, account=account)
    except:
        return None


def account_get_category(account, pk_category):
    try:
        return Category.objects.get(id=pk_category,
                                    account=account)
    except:
        return None


def account_get_directory(account, pk_category, pk_directory):
    try:
        return Directory.objects.get(id=pk_directory,
                                     category_id=pk_category,
                                     category__account=account)
    except Exception as e:
        print e
        return None


def account_get_filecontainer(account, pk_category, pk_filecontainer):
    try:
        return FileContainer.objects.get(id=pk_filecontainer,
                                         category_id=pk_category,
                                         category__account=account)
    except Exception as e:
        print e
        return None


def account_get_fileindex(account, pk_category, pk_filecontainer, pk_fileindex):
    try:
        return FileContainer.objects.get(id=pk_fileindex,
                                         filecontainer_id=pk_filecontainer,
                                         filecontainer__category_id=pk_category,
                                         filecontainer__category__account=account)
    except:
        return None


# CARD
def card_get_category(card, pk_category):
    try:
        return Category.objects.get(id=pk_category,
                                    card_set=card)
    except:
        return None


def card_get_directory(card, pk_category, pk_directory):
    try:
        return Directory.objects.get(id=pk_directory,
                                     category_id=pk_category,
                                     category__card_set=card)
    except Exception as e:
        print e
        return None


def card_get_filecontainer(card, pk_category, pk_filecontainer):
    try:
        return FileContainer.objects.get(id=pk_filecontainer,
                                         category_id=pk_category,
                                         category__card_set=card)
    except:
        return None


def card_get_fileindex(card, pk_category, pk_filecontainer, pk_fileindex):
    try:
        return FileContainer.objects.get(id=pk_fileindex,
                                         filecontainer_id=pk_filecontainer,
                                         filecontainer__category_id=pk_category,
                                         filecontainer__category__card_set=card)
    except:
        return None