from privcard_server_app.serializers.content import CategorySerializer
from rest_framework import permissions
from privcard_server_app.models.category import Category
from rest_framework.views import APIView
from privcard_server_app.utils.query import auth_get_card
from rest_framework.response import Response
from privcard_server_app.utils.permission import IsCard


class CardCategoryListView(APIView):

    serializer_class = CategorySerializer
    permission_classes = (permissions.IsAuthenticated, IsCard, )

    def get(self, request, *args, **kwargs):
        card = auth_get_card(auth=self.request.user)
        if card:
            return Response(data=self.serializer_class(card.category_set.all(), many=True).data, status=200)
        return Response(data={'error': 'authentication failed'}, status=400)

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        card = auth_get_card(auth=self.request.user)
        if card:
            category = Category()
            category.name = serializer.validated_data['name']
            category.account = card.account
            category.save()
            category.card_set.add(card)
            category.save()
            return Response(data=self.serializer_class(category).data, status=200)
        return Response(data={'error': 'authentication failed'}, status=400)