from privcard_server_app.serializers.content import DirectorySerializer
from rest_framework import permissions
from privcard_server_app.models.directory import Directory
from privcard_server_app.utils.query import auth_get_card, card_get_directory
from rest_framework.response import Response
from rest_framework.views import APIView
from privcard_server_app.utils.permission import IsAccount


class CardDirectoryListView(APIView):

    serializer_class = DirectorySerializer
    permission_classes = (permissions.IsAuthenticated, IsAccount, )

    def get(self, request, *args, **kwargs):
        card = auth_get_card(auth=self.request.user)
        if card:
            category = card_get_directory(card=card,
                                          pk_category=kwargs['pk_category'])
            if category:
                return Response(data=self.serializer_class(category.directory_set.all(), many=True).data, status=200)
            else:
                return Response(data={'error': 'container query failed'}, status=400)
        return Response(data={'error': 'authentication failed'}, status=400)

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        card = auth_get_card(auth=self.request.user)
        if card:
            category = card_get_directory(card=card,
                                          pk_category=kwargs['pk_category'])
            if category:
                directory = Directory()
                directory.name = serializer.validated_data['name']
                directory.category = category
                directory.save()
                return Response(data=self.serializer_class(directory).data, status=200)
            else:
                return Response(data={'error': 'container query failed'}, status=400)
        return Response(data={'error': 'authentication failed'}, status=400)
