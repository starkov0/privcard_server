from privcard_server_app.serializers.content import FileContainerSerializer
from rest_framework import permissions
from privcard_server_app.models.fileContainer import FileContainer
from privcard_server_app.utils.query import auth_get_card, card_get_category
from rest_framework.response import Response
from rest_framework.views import APIView
from privcard_server_app.utils.permission import IsAccount


class CardFileContainerListView(APIView):

    serializer_class = FileContainerSerializer
    permission_classes = (permissions.IsAuthenticated, IsAccount, )

    def get(self, request, *args, **kwargs):
        card = auth_get_card(auth=self.request.user)
        if card:
            category = card_get_category(card=card,
                                         pk_category=kwargs['pk_category'])
            if category:
                return Response(data=self.serializer_class(category.filecontainer_set.all(), many=True).data, status=200)
            else:
                return Response(data={'error': 'container query failed'}, status=400)
        return Response(data={'error': 'authentication failed'}, status=400)

    def post(self, request, *args, **kwargs):
        serializer = FileContainerSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        card = auth_get_card(auth=self.request.user)
        if card:
            category = card_get_category(card=card,
                                         pk_category=kwargs['pk_category'])
            if category:
                filecontainer = FileContainer()
                filecontainer.name = serializer.validated_data['name']
                filecontainer.path = serializer.validated_data['path']
                filecontainer.category = category
                filecontainer.save()
                return Response(data=self.serializer_class(filecontainer).data, status=200)
            else:
                return Response(data={'error': 'container query failed'}, status=400)
        return Response(data={'error': 'authentication failed'}, status=400)
