from privcard_server_app.serializers.card import CardCardSerializer
from rest_framework import permissions
from rest_framework.response import Response
from privcard_server_app.utils.permission import IsCard
from privcard_server_app.utils.query import auth_get_card
from rest_framework.views import APIView


class CardView(APIView):

    serializer_class = CardCardSerializer
    permission_classes = (permissions.IsAuthenticated, IsCard, )

    def get(self, request, *args, **kwargs):
        card = auth_get_card(auth=request.user)
        if card:
            return Response(data=self.serializer_class(card).data, status=200)
        else:
            return Response(data={'error': 'authentication failed'}, status=400)