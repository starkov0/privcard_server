from rest_framework.views import APIView
from privcard_server_app.serializers.auth import LoginSerializer
from privcard_server_app.serializers.account import AccountSerializer
from privcard_server_app.serializers.card import CardSerializer
from rest_framework import permissions
from privcard_server_app.utils.token import create_token
from rest_framework.response import Response
from privcard_server_app.utils.query import auth_get_account, auth_get_card
from privcard_server_app.utils.auth import authenticate


class LoginView(APIView):

    serializer_class = LoginSerializer
    permission_classes = (permissions.AllowAny, )

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        auth = authenticate(username=serializer.validated_data['username'],
                            password=serializer.validated_data['password'])
        if auth:
            try:
                token = create_token(user=auth)
            except Exception as e:
                return Response(data={'error': e.message}, status=400)
            account = auth_get_account(auth=auth)
            if account:
                return Response(data={'account': AccountSerializer(account).data, 'token': token.key}, status=200)
            card = auth_get_card(auth=auth)
            if card:
                return Response(data={'card': CardSerializer(card).data, 'token': token.key}, status=200)
        return Response(data={'error': 'authentication failed'}, status=400)