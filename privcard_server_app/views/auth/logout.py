from rest_framework.views import APIView
from rest_framework.response import Response
from privcard_server_app.utils.token import remove_token
from rest_framework import permissions


class LogoutView(APIView):

    permission_classes = (permissions.IsAuthenticated, )

    def post(self, request, *args, **kwargs):
        try:
            remove_token(user=request.user)
            return Response(data={'success': 'logout'}, status=200)
        except Exception as e:
            return Response(data={'error': e.message}, status=400)