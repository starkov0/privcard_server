from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from privcard_server_app.models.auth import Auth


# Account
class AnonymousUsernameView(APIView):

    permission_classes = (permissions.AllowAny, )

    def get(self, request, *args, **kwargs):
        try:
            Auth.objects.get(username=kwargs['username'])
            return Response(data='username taken', status=200)
        except:
            return Response(data='username free', status=200)