from privcard_server_app.serializers.squestion import SQuestionSerializer
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from privcard_server_app.utils.query import get_account, account_get_squestion


class AnonymousPasswordView(APIView):

    serializer_class = SQuestionSerializer
    permission_classes = (permissions.AllowAny, )

    def get(self, request, *args, **kwargs):
        account = get_account(username=kwargs['username'])
        if account:
            return Response(data=self.serializer_class(account.squestion_set.all(), many=True).data, status=200)
        else:
            return Response(data={'error': 'invalid username'}, status=400)

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        account = get_account(username=kwargs['username'])
        if account:
            squestion = account_get_squestion(pk_squestion=serializer.validated_data['id'], account=account)
            if squestion:
                if squestion.check_answer(raw_answer=serializer.validated_data['answer']):
                    account.set_password(raw_password=serializer.validated_data['password'])
                    account.save()
                    return Response(data={'success': 'password updated'}, status=200)
                else:
                    return Response(data={'error': 'wrong answer'}, status=400)
            else:
                return Response(data={'success': 'invalid answer id'}, status=400)
        else:
            return Response(data={'error': 'invalid username'}, status=400)