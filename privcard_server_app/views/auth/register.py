from rest_framework.views import APIView
from privcard_server_app.serializers.auth import RegisterSerializer
from privcard_server_app.serializers.account import AccountSerializer
from rest_framework import permissions
from privcard_server_app.utils.token import create_token
from privcard_server_app.models.account import Account
from rest_framework.response import Response


class RegisterView(APIView):

    serializer_class = RegisterSerializer
    permission_classes = (permissions.AllowAny, )

    # creates and logs in Account
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        if not serializer.validated_data['username']:
            return Response(data={'error': 'username is empty'}, status=400)

        try:
            account = Account()
            account.username = serializer.validated_data['username']
            account.set_password(raw_password=serializer.validated_data['password'])
            account.password_key = serializer.validated_data['password_key']
            account.save()
        except Exception as e:
            return Response(data={'error': e.message}, status=400)

        try:
            token = create_token(user=account)
            return Response(data={'account': AccountSerializer(account).data, 'token': token.key}, status=200)
        except Exception as e:
            return Response(data={'error': e.message}, status=400)