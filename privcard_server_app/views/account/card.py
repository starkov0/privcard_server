from privcard_server_app.serializers.card import CardSerializer
from rest_framework import permissions
from rest_framework.views import APIView
from privcard_server_app.utils.permission import IsAccount
from privcard_server_app.utils.query import auth_get_account, account_get_card, account_get_category
from rest_framework.response import Response


class AccountCardView(APIView):

    serializer_class = CardSerializer
    permission_classes = (permissions.IsAuthenticated, IsAccount, )

    def get(self, request, *args, **kwargs):
        account = auth_get_account(auth=request.user)
        if account:
            card = account_get_card(account=account, pk_card=kwargs['pk_card'])
            if card:
                return Response(data=CardSerializer(card).data, status=200)
            else:
                return Response(data={'error': 'card query failed'}, status=400)
        else:
            return Response(data={'error': 'authentication failed'}, status=400)

    def put(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        account = auth_get_account(auth=request.user)
        if account:
            card = account_get_card(account=account, pk_card=kwargs['pk_card'])
            if card:
                # name
                card.name = serializer.validated_data['name']
                card.save()
                # category
                card.category_set.clear()
                for category_dict in serializer.validated_data['category_set']:
                    category = account_get_category(account, category_dict['id'])
                    card.category_set.add(category)
                return Response(data=CardSerializer(card).data, status=200)
            else:
                return Response(data={'error': 'card query failed'}, status=400)
        else:
            return Response(data={'error': 'authentication failed'}, status=400)

    def delete(self, request, *args, **kwargs):
        account = auth_get_account(auth=request.user)
        if account:
            card = account_get_card(account=account, pk_card=kwargs['pk_card'])
            if card:
                card.delete()
                return Response(data={'success': 'card deleted'}, status=200)
            else:
                return Response(data={'error': 'card query failed'}, status=400)
        else:
            return Response(data={'error': 'authentication failed'}, status=400)
