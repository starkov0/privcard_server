from privcard_server_app.serializers.content import CategorySerializer
from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from privcard_server_app.utils.query import auth_get_account, account_get_category
from privcard_server_app.utils.permission import IsAccount


class AccountCategoryView(APIView):

    serializer_class = CategorySerializer
    permission_classes = (permissions.IsAuthenticated, IsAccount, )

    def get(self, request, *args, **kwargs):
        account = auth_get_account(auth=self.request.user)
        if account:
            category = account_get_category(account=account,
                                            pk_category=kwargs['pk_category'])
            if category:
                return Response(data=self.serializer_class(category).data, status=200)
            else:
                return Response(data={'error': 'category query failed'}, status=400)
        return Response(data={'error': 'authentication failed'}, status=400)

    def put(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        account = auth_get_account(auth=self.request.user)
        if account:
            category = account_get_category(account=account,
                                            pk_category=kwargs['pk_category'])
            if category:
                category.name = serializer.validated_data['name']
                category.save()
                return Response(data=self.serializer_class(category).data, status=200)
            else:
                return Response(data={'error': 'category query failed'}, status=400)
        return Response(data={'error': 'authentication failed'}, status=400)

    def delete(self, request, *args, **kwargs):
        account = auth_get_account(auth=self.request.user)
        if account:
            category = account_get_category(account=account,
                                            pk_category=kwargs['pk_category'])
            if category:
                category.delete()
                return Response(data={'success': 'category deleted'}, status=200)
            else:
                return Response(data={'error': 'category query failed'}, status=400)
        return Response(data={'error': 'authentication failed'}, status=400)