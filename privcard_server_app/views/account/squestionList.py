from privcard_server_app.serializers.squestion import SQuestionSerializer
from rest_framework import permissions
from privcard_server_app.utils.permission import IsAccount
from rest_framework.response import Response
from privcard_server_app.utils.query import auth_get_account
from rest_framework.views import APIView
from privcard_server_app.models.squestion import SQuestion


class AccountSQuestionListView(APIView):

    serializer_class = SQuestionSerializer
    permission_classes = (permissions.IsAuthenticated, IsAccount, )

    def get(self, request, *args, **kwargs):
        account = auth_get_account(auth=request.user)
        if account:
            return Response(data=self.serializer_class(account.squestion_set.all(), many=True).data, status=200)
        else:
            return Response(data={'error': 'authentication failed'}, status=400)

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        account = auth_get_account(auth=request.user)
        if account:
            squestion = SQuestion()
            squestion.question = serializer.validated_data['question']
            squestion.set_answer(serializer.validated_data['answer'])
            squestion.answer_key = serializer.validated_data['answer_key']
            squestion.account = account
            squestion.save()
            return Response(data=self.serializer_class(squestion).data, status=200)
        else:
            return Response(data={'error': 'authentication failed'}, status=400)