from privcard_server_app.serializers.content import DirectorySerializer
from rest_framework import permissions
from privcard_server_app.models.directory import Directory
from privcard_server_app.utils.query import auth_get_account, account_get_category
from rest_framework.response import Response
from rest_framework.views import APIView
from privcard_server_app.utils.permission import IsAccount


class AccountDirectoryListView(APIView):

    serializer_class = DirectorySerializer
    permission_classes = (permissions.IsAuthenticated, IsAccount, )

    def get(self, request, *args, **kwargs):
        account = auth_get_account(auth=self.request.user)
        if account:
            category = account_get_category(account=account,
                                            pk_category=kwargs['pk_category'])
            if category:
                return Response(data=self.serializer_class(category.directory_set.all(), many=True).data, status=200)
            else:
                return Response(data={'error': 'container query failed'}, status=400)
        return Response(data={'error': 'authentication failed'}, status=400)

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        account = auth_get_account(auth=self.request.user)
        if account:
            category = account_get_category(account=account,
                                            pk_category=kwargs['pk_category'])
            if category:
                directory = Directory()
                directory.name = serializer.validated_data['name']
                directory.category = category
                directory.save()
                try:
                    print self.serializer_class(directory).data
                except Exception as e:
                    print e
                return Response(data=self.serializer_class(directory).data, status=200)
            else:
                return Response(data={'error': 'container query failed'}, status=400)
        return Response(data={'error': 'authentication failed'}, status=400)
