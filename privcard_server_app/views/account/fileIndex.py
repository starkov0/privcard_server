from privcard_server_app.serializers.content import FileIndexSerializer
from rest_framework import permissions
from privcard_server_app.utils.query import auth_get_account, account_get_fileindex
from rest_framework.response import Response
from rest_framework.views import APIView
from privcard_server_app.utils.permission import IsAccount


class AccountFileIndexView(APIView):

    serializer_class = FileIndexSerializer
    permission_classes = (permissions.IsAuthenticated, IsAccount, )

    def get(self, request, *args, **kwargs):
        account = auth_get_account(auth=self.request.user)
        if account:
            fileindex = account_get_fileindex(account=account,
                                              pk_category=kwargs['pk_category'],
                                              pk_filecontainer=kwargs['pk_filecontainer'],
                                              pk_fileindex=kwargs['pk_fileindex'])
            if fileindex:
                return Response(data=self.serializer_class(fileindex).data, status=200)
            else:
                return Response(data={'error': 'fileindex query failed'}, status=400)
        return Response(data={'error': 'authentication failed'}, status=400)

