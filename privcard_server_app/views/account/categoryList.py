from privcard_server_app.serializers.content import CategorySerializer
from rest_framework import permissions
from privcard_server_app.models.category import Category
from rest_framework.views import APIView
from privcard_server_app.utils.query import auth_get_account
from rest_framework.response import Response
from privcard_server_app.utils.permission import IsAccount


class AccountCategoryListView(APIView):

    serializer_class = CategorySerializer
    permission_classes = (permissions.IsAuthenticated, IsAccount, )

    def get(self, request, *args, **kwargs):
        account = auth_get_account(auth=self.request.user)
        if account:
            return Response(data=self.serializer_class(account.category_set.all(), many=True).data, status=200)
        return Response(data={'error': 'authentication failed'}, status=400)

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        account = auth_get_account(auth=self.request.user)
        if account:
            category = Category()
            category.name = serializer.validated_data['name']
            category.account = account
            category.save()
            return Response(data=self.serializer_class(category).data, status=200)
        return Response(data={'error': 'authentication failed'}, status=400)