from privcard_server_app.serializers.card import CardSerializer
from rest_framework import permissions
from privcard_server_app.models.card import Card
from privcard_server_app.utils.permission import IsAccount
from privcard_server_app.utils.query import auth_get_account
from rest_framework.response import Response
from rest_framework.views import APIView


class AccountCardListView(APIView):

    serializer_class = CardSerializer
    permission_classes = (permissions.IsAuthenticated, IsAccount, )

    def get(self, request, *args, **kwargs):
        account = auth_get_account(auth=request.user)
        if account:
            return Response(data=CardSerializer(account.card_set.all(), many=True).data, status=200)
        else:
            return Response(data={'error': 'authentication failed'}, status=400)

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        account = auth_get_account(auth=request.user)
        if account:
            try:
                card = Card()
                card.name = serializer.validated_data['name']
                card.username = serializer.validated_data['username']
                card.set_password(raw_password=serializer.validated_data['password'])
                card.password_key = serializer.validated_data['password_key']
                card.key_password = serializer.validated_data['key_password']
                card.account = account
                card.save()
                return Response(data=CardSerializer(card).data, status=200)
            except Exception as e:
                return Response(data={'error': e.message}, status=400)
        else:
            return Response(data={'error': 'authentication failed'}, status=400)