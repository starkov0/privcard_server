from privcard_server_app.serializers.content import FileContainerSerializer
from rest_framework import permissions
from rest_framework.views import APIView
from privcard_server_app.utils.query import auth_get_account, account_get_filecontainer
from rest_framework.response import Response
from privcard_server_app.utils.permission import IsAccount


class AccountFileContainerView(APIView):

    serializer_class = FileContainerSerializer
    permission_classes = (permissions.IsAuthenticated, IsAccount, )

    def get(self, request, *args, **kwargs):
        account = auth_get_account(auth=self.request.user)
        if account:
            filecontainer = account_get_filecontainer(account=account,
                                                      pk_category=kwargs['pk_category'],
                                                      pk_filecontainer=kwargs['pk_filecontainer'])
            if filecontainer:
                return Response(data=self.serializer_class(filecontainer).data, status=200)
            else:
                return Response(data={'error': 'filecontainer query failed'}, status=400)
        return Response(data={'error': 'authentication failed'}, status=400)

    def put(self, request, *args, **kwargs):
        serializer = FileContainerSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        account = auth_get_account(auth=self.request.user)
        if account:
            filecontainer = account_get_filecontainer(account=account,
                                                      pk_category=kwargs['pk_category'],
                                                      pk_filecontainer=kwargs['pk_filecontainer'])
            if filecontainer:
                filecontainer.name = serializer.validated_data['name']
                filecontainer.save()
                return Response(data=self.serializer_class(filecontainer).data, status=200)
            else:
                return Response(data={'error': 'filecontainer query failed'}, status=400)
        return Response(data={'error': 'authentication failed'}, status=400)

    def delete(self, request, *args, **kwargs):
        account = auth_get_account(auth=self.request.user)
        if account:
            filecontainer = account_get_filecontainer(account=account,
                                                      pk_category=kwargs['pk_category'],
                                                      pk_filecontainer=kwargs['pk_filecontainer'])
            if filecontainer:
                filecontainer.delete()
                return Response(data={'success': 'filecontainer deleted'}, status=200)
            else:
                return Response(data={'error': 'filecontainer query failed'}, status=400)
        return Response(data={'error': 'authentication failed'}, status=400)