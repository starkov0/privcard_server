from privcard_server_app.serializers.account import AccountSerializer
from rest_framework import permissions
from privcard_server_app.utils.permission import IsAccount
from rest_framework.response import Response
from privcard_server_app.utils.query import auth_get_account
from rest_framework.views import APIView


class AccountView(APIView):

    serializer_class = AccountSerializer
    permission_classes = (permissions.IsAuthenticated, IsAccount, )

    def get(self, request, *args, **kwargs):
        account = auth_get_account(auth=request.user)
        if account:
            return Response(data=self.serializer_class(account).data, status=200)
        else:
            return Response(data={'error': 'authentication failed'}, status=400)

    def put(self, request, *args, **kwargs):
        serializer = AccountSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        account = auth_get_account(auth=request.user)
        if account:
            account.username = serializer.validated_data['username']
            account.set_password(serializer.validated_data['password'])
            account.password_key = serializer.validated_data['password_key']
            try:
                account.save()
            except Exception as e:
                return Response(data=e.message, status=400)
            return Response(data=AccountSerializer(account).data, status=200)
        else:
            return Response(data={'error': 'authentication failed'}, status=400)

    def delete(self, request, *args, **kwargs):
        account = auth_get_account(auth=request.user)
        if account:
            account.delete()
            return Response(data={'success': 'account deleted'}, status=200)
        else:
            return Response(data={'error': 'authentication failed'}, status=400)