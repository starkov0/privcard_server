from privcard_server_app.serializers.content import DirectorySerializer
from rest_framework import permissions
from rest_framework.views import APIView
from privcard_server_app.utils.query import auth_get_account, account_get_directory
from rest_framework.response import Response
from privcard_server_app.utils.permission import IsAccount


class AccountDirectoryView(APIView):

    serializer_class = DirectorySerializer
    permission_classes = (permissions.IsAuthenticated, IsAccount, )

    def get(self, request, *args, **kwargs):
        account = auth_get_account(auth=self.request.user)
        if account:
            directory = account_get_directory(account=account,
                                              pk_category=kwargs['pk_category'],
                                              pk_directory=kwargs['pk_directory'])
            if directory:
                return Response(data=self.serializer_class(directory).data, status=200)
            else:
                return Response(data={'error': 'directory query failed'}, status=400)
        return Response(data={'error': 'authentication failed'}, status=400)

    def put(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        account = auth_get_account(auth=self.request.user)
        if account:
            directory = account_get_directory(account=account,
                                              pk_category=kwargs['pk_category'],
                                              pk_directory=kwargs['pk_directory'])
            if directory:
                directory.name = serializer.validated_data['name']
                directory.save()
                return Response(data=self.serializer_class(directory).data, status=200)
            else:
                return Response(data={'error': 'directory query failed'}, status=400)
        return Response(data={'error': 'authentication failed'}, status=400)

    def delete(self, request, *args, **kwargs):
        account = auth_get_account(auth=self.request.user)
        if account:
            directory = account_get_directory(account=account,
                                              pk_category=kwargs['pk_category'],
                                              pk_directory=kwargs['pk_directory'])
            if directory:
                directory.delete()
                return Response(data={'success': 'directory deleted'}, status=200)
            else:
                return Response(data={'error': 'directory query failed'}, status=400)
        return Response(data={'error': 'authentication failed'}, status=400)