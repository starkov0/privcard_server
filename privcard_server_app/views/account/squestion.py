from privcard_server_app.serializers.squestion import SQuestionSerializer
from rest_framework import permissions
from privcard_server_app.utils.permission import IsAccount
from rest_framework.response import Response
from privcard_server_app.utils.query import auth_get_account, account_get_squestion
from rest_framework.views import APIView


class AccountSQuestionView(APIView):

    serializer_class = SQuestionSerializer
    permission_classes = (permissions.IsAuthenticated, IsAccount, )

    def get(self, request, *args, **kwargs):
        account = auth_get_account(auth=request.user)
        if account:
            squestion = account_get_squestion(kwargs['pk_squestion'], account)
            if squestion:
                return Response(data=self.serializer_class(squestion).data, status=200)
            else:
                return Response(data={'error': 'squestion query failed'}, status=400)
        else:
            return Response(data={'error': 'authentication failed'}, status=400)

    def delete(self, request, *args, **kwargs):
        account = auth_get_account(auth=request.user)
        if account:
            squestion = account_get_squestion(kwargs['pk_squestion'], account)
            if squestion:
                squestion.delete()
                return Response(data={'success': 'squestion deleted'}, status=200)
            else:
                return Response(data={'error': 'squestion query failed'}, status=400)
        else:
            return Response(data={'error': 'authentication failed'}, status=400)