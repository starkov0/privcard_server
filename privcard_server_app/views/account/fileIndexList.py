from privcard_server_app.serializers.content import FileIndexSerializer, FileIndexIdSerializer
from rest_framework import permissions
from privcard_server_app.utils.query import auth_get_account, account_get_filecontainer
from rest_framework.response import Response
from rest_framework.views import APIView
from privcard_server_app.utils.permission import IsAccount
from privcard_server_app.models.fileIndex import FileIndex


class AccountFileIndexListView(APIView):

    serializer_class = FileIndexSerializer
    permission_classes = (permissions.IsAuthenticated, IsAccount, )

    def get(self, request, *args, **kwargs):
        account = auth_get_account(auth=self.request.user)
        if account:
            filecontainer = account_get_filecontainer(account=account,
                                                      pk_category=kwargs['pk_category'],
                                                      pk_filecontainer=kwargs['pk_filecontainer'])
            if filecontainer:
                return Response(data=self.serializer_class(filecontainer.fileindex_set.all(), many=True).data, status=200)
            else:
                return Response(data={'error': 'filecontainer query failed'}, status=400)
        return Response(data={'error': 'authentication failed'}, status=400)

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        account = auth_get_account(auth=self.request.user)
        if account:
            filecontainer = account_get_filecontainer(account=account,
                                                      pk_category=kwargs['pk_category'],
                                                      pk_filecontainer=kwargs['pk_filecontainer'])
            if filecontainer:
                fileindex = FileIndex()
                fileindex.index = serializer.validated_data['index']
                fileindex.file = serializer.validated_data['file']
                fileindex.filecontainer = filecontainer
                fileindex.category = filecontainer.category
                fileindex.save()
                return Response(data=FileIndexIdSerializer(fileindex).data, status=200)
            else:
                return Response(data={'error': 'filecontainer query failed'}, status=400)
        return Response(data={'error': 'authentication failed'}, status=400)
