# from django.conf.urls import url

from privcard_server_app.views.auth.register import RegisterView
from privcard_server_app.views.auth.login import LoginView
from privcard_server_app.views.auth.logout import LogoutView

from privcard_server_app.views.account.account import AccountView
from privcard_server_app.views.account.squestion import AccountSQuestionView
from privcard_server_app.views.account.squestionList import AccountSQuestionListView
from privcard_server_app.views.account.card import AccountCardView
from privcard_server_app.views.account.cardList import AccountCardListView
from privcard_server_app.views.account.category import AccountCategoryView
from privcard_server_app.views.account.categoryList import AccountCategoryListView

from privcard_server_app.views.account.directory import AccountDirectoryView
from privcard_server_app.views.account.directoryList import AccountDirectoryListView

from privcard_server_app.views.account.fileContainer import AccountFileContainerView
from privcard_server_app.views.account.fileContainerList import AccountFileContainerListView
from privcard_server_app.views.account.fileIndex import AccountFileIndexView
from privcard_server_app.views.account.fileIndexList import AccountFileIndexListView

from privcard_server_app.views.auth.username import AnonymousUsernameView
from privcard_server_app.views.auth.password import AnonymousPasswordView

from privcard_server_app.views.card.card import CardView
from privcard_server_app.views.card.category import CardCategoryView
from privcard_server_app.views.card.categoryList import CardCategoryListView

from privcard_server_app.views.card.directory import CardDirectoryView
from privcard_server_app.views.card.directoryList import CardDirectoryListView

from privcard_server_app.views.card.fileContainer import CardFileContainerView
from privcard_server_app.views.card.fileContainerList import CardFileContainerListView
from privcard_server_app.views.card.fileIndex import CardFileIndexView
from privcard_server_app.views.card.fileIndexList import CardFileIndexListView


# pk_directory

from django.conf.urls import url

urlpatterns = [
    url(r'^login$', LoginView.as_view()),
    url(r'^logout$', LogoutView.as_view()),
    url(r'^register$', RegisterView.as_view()),

    url(r'^account$', AccountView.as_view()),
    url(r'^account/'
        r'card$', AccountCardListView.as_view()),
    url(r'^account/'
        r'squestion$', AccountSQuestionListView.as_view()),
        url(r'^account/'
        r'squestion/(?P<pk_squestion>[0-9]+)$', AccountSQuestionView.as_view()),
    url(r'^account/'
        r'card/(?P<pk_card>[0-9]+)$', AccountCardView.as_view()),
    url(r'^account/'
        r'category$', AccountCategoryListView.as_view()),
    url(r'^account/'
        r'category/(?P<pk_category>[0-9]+)$', AccountCategoryView.as_view()),
    url(r'^account/'
        r'category/(?P<pk_category>[0-9]+)/'
        r'directory$', AccountDirectoryListView.as_view()),
    url(r'^account/'
        r'category/(?P<pk_category>[0-9]+)/'
        r'directory/(?P<pk_directory>[0-9]+)$', AccountDirectoryView.as_view()),
    url(r'^account/'
        r'category/(?P<pk_category>[0-9]+)/'
        r'filecontainer$', AccountFileContainerListView.as_view()),
    url(r'^account/'
        r'category/(?P<pk_category>[0-9]+)/'
        r'filecontainer/(?P<pk_filecontainer>[0-9]+)$', AccountFileContainerView.as_view()),
    url(r'^account/'
        r'category/(?P<pk_category>[0-9]+)/'
        r'filecontainer/(?P<pk_filecontainer>[0-9]+)/'
        r'fileindex$', AccountFileIndexListView.as_view()),
    url(r'^account/'
        r'category/(?P<pk_category>[0-9]+)/'
        r'filecontainer/(?P<pk_filecontainer>[0-9]+)/'
        r'fileindex/(?P<pk_fileIndex>[0-9]+)$', AccountFileIndexView.as_view()),

    url(r'^anonymous/username/(?P<username>.+)$', AnonymousUsernameView.as_view()),
    url(r'^anonymous/password/(?P<username>.+)$', AnonymousPasswordView.as_view()),

    url(r'^card$', CardView.as_view()),
    url(r'^card/'
        r'category$', CardCategoryListView.as_view()),
    url(r'^card/'
        r'category/(?P<pk_category>[0-9]+)$', CardCategoryView.as_view()),
    url(r'^card/'
        r'category/(?P<pk_category>[0-9]+)$', CardCategoryView.as_view()),

    url(r'^card/'
        r'category/(?P<pk_category>[0-9]+)/'
        r'directory$', CardDirectoryListView.as_view()),
    url(r'^card/'
        r'category/(?P<pk_category>[0-9]+)/'
        r'directory/(?P<pk_directory>[0-9]+)$', CardDirectoryView.as_view()),

    url(r'^card/'
        r'category/(?P<pk_category>[0-9]+)/'
        r'filecontainer$', CardFileContainerListView.as_view()),
    url(r'^card/'
        r'category/(?P<pk_category>[0-9]+)/'
        r'filecontainer/(?P<pk_filecontainer>[0-9]+)$', CardFileContainerView.as_view()),
    url(r'^card/'
        r'category/(?P<pk_category>[0-9]+)/'
        r'filecontainer/(?P<pk_filecontainer>[0-9]+)/'
        r'fileindex$', CardFileIndexListView.as_view()),
    url(r'^card/'
        r'category/(?P<pk_category>[0-9]+)/'
        r'filecontainer/(?P<pk_filecontainer>[0-9]+)/'
        r'fileindex/(?P<pk_fileIndex>[0-9]+)$', CardFileIndexView.as_view()),
]